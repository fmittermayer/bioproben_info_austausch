# BIS variables
## Mandatory
### controlled
    - Main user
    - Gear
    - Gear Configuration
    - Collection
    - PI
    - Sample container
    - Fixation
    - StorageTemperature
    - SampleType
    - SampleTypeSpecific
    - Sphere
    - Sample Context

### uncontrolled
    - Date start
    - Lat start
    - Long start
    - Storage ID
    - Nagoya Case number
### For experiments
    - TreatmentN
    - TreatmentLevelN

## Recommended
### controlled
    - Campaign
    - FunktionalGroup
    - TaxonomicGroup/Species

### uncontolled
    - Time start
    - Timezone
    - Time stop
    - Water depth start
    - Water depth stop
    - Sampling depth start
    - Sampling depth stop
    - station ID
    - Subevent
    - Lat stop
    - Long stop
    - Aphia ID

## Additional
    - Custom ID
    - Replicate number
    - various links via URLs
    - .....
