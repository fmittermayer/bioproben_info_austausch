# Austausch_zu_Bioproben

Hier werden die Infos zu den diversen Fragen zu Bioproben abgelegt und bearbeitet. 

## Probentypen
In der bio_sampletypes.csv werden die verschiedenen Probentypen beschrieben und aufgelistet. Dabei ist die Struktur der "Überkategorie" SampleTyp und der feineren Auslösung SampleTypeSpecific zu beachten. Im Kommentarfeld können detailierte Beschreibungen bei Bedarf gegen werden. 

## Sphere_Layer
In der Sphere_Layer.csv werden die verschiedenen Probenkategorien aufgelistet, die im Viewer als Layer erscheinen sollen. 
